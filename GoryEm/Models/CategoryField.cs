using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoryEm.Models
{
    public class CategoryField
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("CategoryId")]
        public virtual int CategoryId {get;set;}

        public CategoryField(string name) 
        {
            Name = name;
        }
        public CategoryField(string name, int cid) 
        {
            Name = name;
            CategoryId = cid;
        }
    }
        
}