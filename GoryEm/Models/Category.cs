using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace GoryEm.Models
{
    public class Category 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public List<CategoryField> Fields { get; set; }
        public Category(string name, bool active) 
        {
            Name = name;
            Active = active;
        }


    }


    
}