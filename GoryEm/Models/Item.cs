using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoryEm.Models
{
    
    public class Item
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string? ImageBase64 {get;set;}
        public bool Active {get;set;}

        [ForeignKey("CategoryId")]
        public virtual int CategoryId { get; set; }
        public List<ItemFieldValue> FieldValues { get; set; }

        public Item(string name, string description, decimal price, bool active, int categoryId)
        {
            Name = name;
            Description = description;
            Price = price;
            Active = active;
            CategoryId = categoryId;
        }
    }
    
}