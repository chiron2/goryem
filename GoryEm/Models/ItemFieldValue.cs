using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoryEm.Models
{
    public class ItemFieldValue
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int FieldId { get; set; }
        public string Value { get; set; }
        [ForeignKey("ItemId")]
        public virtual int ItemId {get;set;}
        [ForeignKey("CategoryId")]
        public virtual int CategoryId {get;set;}

        public ItemFieldValue(int fieldId, string value) 
        {
            FieldId = fieldId;
            Value = value;
        }
        public ItemFieldValue(int fieldId, string value, int category) 
        {
            FieldId = fieldId;
            Value = value;
            CategoryId = category;
        }
    }
        
}