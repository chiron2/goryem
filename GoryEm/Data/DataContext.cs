using Microsoft.EntityFrameworkCore;
using GoryEm.Models;
using System.Reflection;

namespace GoryEm.Data
{
    public class DataContext : DbContext 
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasIndex(c => new { c.Name })
                .IsUnique(true);
            
            modelBuilder.Entity<ItemFieldValue>()
                .HasIndex(ifv => new {ifv.FieldId, ifv.ItemId});

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Cascade;
            } 

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Category> Categories {get; set;}
        public DbSet<CategoryField> CategoryFields {get;set;}
        public DbSet<Item> Items {get;set;}
        public DbSet<ItemFieldValue> ItemFieldValues {get;set;}

    }
}