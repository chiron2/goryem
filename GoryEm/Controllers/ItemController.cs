using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using GoryEm.Data;
using GoryEm.DTO;
using GoryEm.Models;


[Route("api/items")]
[ApiController]
public class ItemController : ControllerBase
{
    private readonly DTOMapper dtoMapper;
    private readonly DataContext _context;
    private readonly ItemReturnDTOMapper itemReturnDTOMapper;

    public ItemController(DataContext context)
    {
        _context = context;
        dtoMapper = new DTOMapper();
        itemReturnDTOMapper = new ItemReturnDTOMapper();
    }

    // Метод для добавление товара используя ItemDTO (модель для реквестов)
    // Тут при заполнении доволнительных полей надо будет вводить ID поля который хотите заполнить в Сваггере видно
    // Так как при методе get сервер отправляет не только название дополнительного поля но и ее ID при добавлении клиентской части с этим проблем не будет 
    [HttpPost]
    public async Task<ActionResult<Item>> CreateItem([FromForm] ItemDTO itemDTO)
    {
        try 
        {
            var doesExistCategory = await _context.Categories.FindAsync(itemDTO.CategoryId);
            if (doesExistCategory == null) 
            {
                return BadRequest("Given new Category id does not exist");
            }
            //Validation for request body (Required fields: name, price)
            if (itemDTO.Name == null || itemDTO.Name.Equals("") || itemDTO.Price == null || itemDTO.Price <= 0) 
            {
                return BadRequest("Wrong request body");
            }
            Item item = dtoMapper.ItemMapper(itemDTO);
            if (itemDTO.ImageBase64 != null && itemDTO.ImageBase64.files != null && itemDTO.ImageBase64.files.Length > 0 )
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await itemDTO.ImageBase64.files.CopyToAsync(ms);
                    byte[] imageBytes = ms.ToArray();
                    item.ImageBase64 = Convert.ToBase64String(imageBytes);
                }
            }
            _context.Items.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtRoute(new { id = item.ItemId }, item);
        }
        catch (DbUpdateException)
        {
            return BadRequest("Failed to create Item.");
        }
        catch (Exception e)
        {
            return BadRequest("Wrong input data" + e);
        }
    }
    //"Удаляет" товар. 
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteItem(int id)
    {
        try
        {
            var existingItem = await _context.Items.FindAsync(id);
            if (existingItem == null) 
            {
                return BadRequest("No item with given ID was found");
            }

            existingItem.Active = false;

            _context.Entry(existingItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        catch (DbUpdateException)
        {
            return BadRequest("Error while updating database");
        }
    }

    // Метод патч при котором нужно будет зполнять измененные поля и отправляет с ID товара, не полученные сервором поля не будут тронуты
    [HttpPatch("{id}")]
    public async Task<IActionResult> UpdateItem(int id, [FromForm] ItemDTO itemDTO)
    {
        try 
        {
            var existingItem = await _context.Items.FindAsync(id);
            if (existingItem == null) 
            {
                return NotFound("No item to update");
            }
            if (itemDTO.FieldValues != null)
            {
                List<ItemFieldValueDTO> itemFieldValueDTOs = itemDTO.FieldValues;
                foreach (ItemFieldValueDTO i in itemFieldValueDTOs) 
                {
                    Console.WriteLine("asdasdds");
                }
                List<ItemFieldValue> itemFieldValues = await _context.ItemFieldValues
                    .Where(ifv => ifv.ItemId == id)
                    .ToListAsync();

                foreach (ItemFieldValue ifv in itemFieldValues) 
                {
                    var newIfv = itemFieldValueDTOs.SingleOrDefault(item => item.FieldId == ifv.FieldId);

                    if (newIfv != null)
                    {
                        ifv.Value = newIfv.Value;
                    }
                }    
            }

            if (itemDTO.Name != null) 
            {
                if (!itemDTO.Name.Equals("")) 
                {
                    existingItem.Name = itemDTO.Name;
                }
            }
            if (itemDTO.Price > 0) 
            {
                existingItem.Price = (decimal)itemDTO.Price;
            }
            if (itemDTO.CategoryId > 0) 
            {
                var doesExistCategory = await _context.Categories.FindAsync(itemDTO.CategoryId);
                if (doesExistCategory == null) 
                {
                    return BadRequest("Given new Category id does not exist");
                }
                existingItem.CategoryId = (int)itemDTO.CategoryId;
                List<ItemFieldValueDTO> itemFieldValueDTOs = itemDTO.FieldValues;
                List<ItemFieldValue> itemFieldValues = await _context.ItemFieldValues
                    .Where(ifv => ifv.ItemId == id)
                    .ToListAsync();

                foreach (ItemFieldValue ifv in itemFieldValues) 
                {
                    var newIfv = itemFieldValueDTOs.SingleOrDefault(item => item.FieldId == ifv.FieldId);

                    if (newIfv != null)
                    {
                        ifv.CategoryId = (int)itemDTO.CategoryId;
                    }
                }    

            }
            if (itemDTO.Description != null) 
            {
                existingItem.Description = itemDTO.Description;
            }
            if (itemDTO.ImageBase64 != null && itemDTO.ImageBase64.files != null && itemDTO.ImageBase64.files.Length > 0 )
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await itemDTO.ImageBase64.files.CopyToAsync(ms);
                    byte[] imageBytes = ms.ToArray();
                    existingItem.ImageBase64 = Convert.ToBase64String(imageBytes);
                }
            }
            _context.Entry(existingItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        catch (DbUpdateConcurrencyException)
        {
            return BadRequest("Concurrency exception occurred");
        }
        catch (DbUpdateException)
        {
            return BadRequest("Failed to update Item");
        }
    }


    [HttpGet("{id}")]
    public async Task<ActionResult<IEnumerable<Item>>> GetItem(int id)
    {
        if (id == null || id <= 0) 
        {
            return BadRequest("Wrong request param");
        }
        var item = await _context.Items
            .Include(i => i.FieldValues) 
            .FirstOrDefaultAsync(i => i.ItemId == id); 

        if (item == null)
        {
            return BadRequest("No Item was found");
        }

        var field = await _context.CategoryFields
            .Where(x => x.CategoryId == item.CategoryId)
            .ToListAsync();

        string categoryName = "";
        var categoryFromDB = await _context.Categories.FindAsync(item.CategoryId);
        categoryName = categoryFromDB.Name;
        
        if (categoryFromDB == null || categoryName == null)
        {
            return BadRequest("An error occured while getting data");
        }

        List<ReturnFieldValues> returnFieldValues = new List<ReturnFieldValues>();
        foreach(ItemFieldValue ifv in item.FieldValues)
        {
            string fieldName = field.SingleOrDefault(x => x.Id == ifv.FieldId)?.Name;

            if (fieldName == null) 
            {
                return BadRequest("An error occured while getting data");
            }
            returnFieldValues.Add(itemReturnDTOMapper.FieldValueMapper(ifv, fieldName));
        }
        ReturnItemDTO returnItemDTO = itemReturnDTOMapper.ItemMapper(item, categoryName, returnFieldValues);
        object result = null;
        if (item.ImageBase64 != null) 
        {
            byte[] bytes = Convert.FromBase64String(item.ImageBase64);
            var imageFileResult = File(bytes, "image/png");
            result = new { Item = returnItemDTO, Image = imageFileResult };
        }
        else
        {
            result = returnItemDTO;
        }
        
        return Ok(result);
    }

    // Список товаров с фильтром
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Item>>> GetItems(
        [FromQuery] Dictionary<string, string> filters)
    {

        IQueryable<Item> query = _context.Items.Where(item => item.Active == true);
        
        if (filters == null || !filters.Any())
        {
            var allItems = await query
                .Include(item => item.FieldValues) 
                .ToListAsync();
            return Ok(allItems);
        }  
        
        foreach (var kvp in filters)
        {
            string key = kvp.Key;
            string value = kvp.Value;
            if (key.Equals("categoryId")) 
            {
                Console.WriteLine(key.Equals("categoryId"));
                if (int.TryParse(value, out int ctg))
                {
                    query = query.Where(x => x.CategoryId == ctg);
                }
                else
                {
                    return BadRequest("Wrong request info");
                }
            }
            else if (key.Equals("name"))
            {
                query = query.Where(x => x.Name.ToUpper().Contains(value.ToUpper()) ||
                    x.Description.ToUpper().Contains(value.ToUpper()));
            }
            else if (key.Equals("endPrice"))
            {
                if (decimal.TryParse(value, out decimal endPrice))
                {
                    query = query.Where(x => x.Price <= endPrice);
                }
                else
                {
                    return BadRequest("Wrong request body");
                }
            } 
            else if (key.Equals("startPrice"))
            {
                if (decimal.TryParse(value, out decimal startPrice))
                {
                    query = query.Where(x => x.Price >= startPrice);
                }
                else
                {
                    return BadRequest("Wrong request body");
                }
            } 
            else
            {
                var cf = await _context.CategoryFields
                    .Where(x => x.Name.ToUpper().Contains(key.ToUpper()))
                    .ToListAsync();

                query = query
                    .Where(item => item.FieldValues.Any(fv => cf.Select(c => c.Id).Contains(fv.FieldId)) &&
                    item.FieldValues.Any(fv => cf.Select(c => c.CategoryId).Contains(item.CategoryId)) && 
                    item.FieldValues.Any(fv => fv.Value.ToUpper().Contains(value.ToUpper())) ||
                    item.Description.ToUpper().Contains(value.ToUpper()));
            }
        }
        var items = await query
            .Include(item => item.FieldValues)
            .ToListAsync();

        return Ok(items);
    }

}