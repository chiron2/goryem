using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using GoryEm.Data;
using GoryEm.DTO;
using GoryEm.Models;

[Route("api/categories")]
[ApiController]
public class CategoriesController : ControllerBase
{
    private readonly DTOMapper dtoMapper;
    private readonly DataContext _context; 

    public CategoriesController(DataContext context)
    {
        _context = context;
        dtoMapper = new DTOMapper();
    }


    // Метод пост для добавления новой категории
    [HttpPost]
    public async Task<ActionResult<Category>> CreateCategory(CategoryDTO category)
    {
        if (category.Name != null && category.Fields != null) 
        {
            try
            {
                Category newCategory = dtoMapper.CategoryMapperOnCreate(category);

                _context.Categories.Add(newCategory);
                await _context.SaveChangesAsync();

                return CreatedAtRoute(new { id = newCategory.CategoryId }, newCategory);
            }
            catch (DbUpdateException)
            {
                return BadRequest("Failed to create category.");
            }
        }
        else 
        {
            return BadRequest("Wrong request form");
        }
    }

    // Удаляет категорию
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCategory(int id) 
    {
        try 
        {
            var existingCategory = await _context.Categories.FindAsync(id);
            if (existingCategory == null) 
            {
                return BadRequest("No category with given ID was found");
            }
            existingCategory.Active = false;

            _context.Entry(existingCategory).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        catch (DbUpdateException) 
        {
            return BadRequest("Error while updating database");
        }
    } 

    // Принимает измененные поля, не полученные с клиентской части поля останутся не тронутыми
    [HttpPatch("{id}")]
    public async Task<IActionResult> UpdateCategory(int id, CategoryDTO category) 
    {
        try 
        {   
            var existingCategory = await _context.Categories.FindAsync(id);
            if (existingCategory == null) 
            {
                return NotFound("No cateogory to update");
            }
            if (!string.IsNullOrWhiteSpace(category.Name))
            {
                existingCategory.Name = category.Name;
            } 
            if (category.Fields != null) 
            {
                List<CategoryFieldDTO> newCfs = category.Fields;
                List<CategoryField> existingCF = await _context.CategoryFields
                    .Where(cf => cf.CategoryId == id)
                    .ToListAsync();

                foreach (CategoryField cf in existingCF)
                {
                    var newCf = newCfs.SingleOrDefault(item => item.Id == cf.Id);

                    if (newCf != null)
                    {
                        cf.Name = newCf.Name;
                    }
                    else
                    {
                        _context.CategoryFields.Remove(cf);
                    }
                }

                foreach (var newCf in newCfs.Where(item => item.Id == null || item.Id == 0 ))
                {
                    var categoryField = new CategoryField(newCf.Name, id);
                    _context.CategoryFields.Add(categoryField);
                }                        
            }


            _context.Entry(existingCategory).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        catch (DbUpdateConcurrencyException)
        {
            return BadRequest("Concurrency exception occurred");
        }
        catch (DbUpdateException)
        {
            return BadRequest("Failed to update Category");
        }
    }

    // Метод для получения всех категории с базы данных
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Category>>> GetCategoriesWithFields()
    {
        var categoriesWithFields = await _context.Categories
            .Where(category => category.Active == true)
            .Include(category => category.Fields) 
            .ToListAsync();

        return Ok(categoriesWithFields);
    }

}
