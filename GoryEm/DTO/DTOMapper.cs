using GoryEm.Models;

namespace GoryEm.DTO
{
    public class DTOMapper
    {
        public Category CategoryMapperOnCreate(CategoryDTO categoryDTO) 
        {
            List<CategoryField> fields = new List<CategoryField>();
            foreach (CategoryFieldDTO field in categoryDTO.Fields)
            {
                CategoryField cf = new CategoryField(field.Name);
                fields.Add(cf);
            }
            Category newCategory = new Category(categoryDTO.Name, true);
            newCategory.Fields = fields;

            return newCategory;
        }
        public Category CategoryMapperWithoutFields(CategoryDTO categoryDTO) 
        {
            Category newCategory = new Category(categoryDTO.Name, true);

            return newCategory;
        }

        public Item ItemMapper(ItemDTO itemDTO) 
        {
            List<ItemFieldValue> fields = new List<ItemFieldValue>();
            foreach (ItemFieldValueDTO field in itemDTO.FieldValues)
            {
                ItemFieldValue newField = new ItemFieldValue(field.FieldId, field.Value, (int)itemDTO.CategoryId);
                fields.Add(newField);
            }
            Item item = new Item(itemDTO.Name, itemDTO.Description, (decimal)itemDTO.Price, true, (int)itemDTO.CategoryId);
            item.FieldValues = fields;

            return item;
        }

    }
}