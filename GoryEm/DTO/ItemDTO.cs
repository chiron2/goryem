namespace GoryEm.DTO
{
    
    public class ItemDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public decimal? Price { get; set; }
        public int? CategoryId { get; set; }
        public FileUpload? ImageBase64 {get;set;}
        public List<ItemFieldValueDTO>? FieldValues { get; set; }
    }
    
}