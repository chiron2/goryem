using GoryEm.Models;

namespace GoryEm.DTO
{
    public class ItemReturnDTOMapper
    {
        public ReturnItemDTO ItemMapper(Item item, string CategoryName, List<ReturnFieldValues> returnFieldValues) 
        {
            ReturnItemDTO returnItemDTO = new ReturnItemDTO();
            returnItemDTO.ItemId = item.ItemId;
            returnItemDTO.Name = item.Name;
            returnItemDTO.Description = item.Description;
            returnItemDTO.Price = item.Price;
            returnItemDTO.Active = item.Active;
            returnItemDTO.CategoryId = item.CategoryId;
            returnItemDTO.CategoryName = CategoryName;
            returnItemDTO.FieldValues = returnFieldValues;

            return returnItemDTO;
        }

        public ReturnFieldValues FieldValueMapper(ItemFieldValue ifv, string fieldName_string)
        {
            ReturnFieldValues rfv = new ReturnFieldValues();
            rfv.Id = ifv.Id;
            rfv.FieldId = ifv.FieldId;
            rfv.Value = ifv.Value;
            rfv.ItemId = ifv.ItemId;
            rfv.FieldName = fieldName_string;

            return rfv;
        }
    }
}