namespace GoryEm.DTO
{
    public class ReturnItemDTO
    {
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool Active {get;set;}
        public int CategoryId { get; set; }
        public string CategoryName {get;set;}
        public List<ReturnFieldValues> FieldValues {get;set;} 
    }
}