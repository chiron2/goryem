namespace GoryEm.DTO
{
    public class ReturnFieldValues
    {
        public int Id {get;set;}
        public string FieldName {get;set;}
        public int FieldId { get; set; }
        public string Value { get; set; }
        public int ItemId {get;set;}
    }
}