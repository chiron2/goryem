namespace GoryEm.DTO
{
    public class ItemFieldValueDTO
    {
        public int FieldId { get; set; }
        public string Value { get; set; }
    }
        
}