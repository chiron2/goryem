namespace GoryEm.DTO
{
    public class CategoryDTO 
    {
        public string? Name { get; set; }
        public List<CategoryFieldDTO>? Fields { get; set; }

    }
    
}